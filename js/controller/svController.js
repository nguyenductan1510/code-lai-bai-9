function informationFromSample() {
  var _maSv = document.getElementById("txtMaSV").value;
  var _tenSv = document.getElementById("txtTenSV").value;
  var _email = document.getElementById("txtEmail").value;
  var _matKhau = document.getElementById("txtPass").value;
  var _diemToan = document.getElementById("txtDiemToan").value * 1;
  var _diemLy = document.getElementById("txtDiemLy").value * 1;
  var _diemHoa = document.getElementById("txtDiemHoa").value * 1;
  var sv = new SinhVien(
    _maSv,
    _tenSv,
    _email,
    _matKhau,
    _diemToan,
    _diemLy,
    _diemHoa
  );
  return sv;
}

function renderDSSV(svArr) {
  var contentHTML = "";
  for (var index = 0; index < svArr.length; index++) {
    var item = svArr[index];
    var contentTr = `   <tr> 
                           <td>${item.ma}</td>
                           <td>${item.ten}</td>
                           <td>${item.email}</td>
                           <td>${item.tinhDTB()}</td>
                           <td> 
                           <button onclick = "deleteStudent('${
                             item.ma
                           }')" class="btn btn-danger">Xóa</button>
                           <button onclick = "studentCorrection('${
                             item.ma
                           }')" class="btn btn-warning ">Sửa</button>
                           </td>
                        </tr>`;
    contentHTML += contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function locationSearch(id, arr) {
  var viTri = -1;

  for (var index = 0; index < arr.length; index++) {
    var sv = arr[index];
    if (sv.ma == id) {
      viTri = index;
      break;
    }
  }
  return viTri;
}

function sampleInformationDemonstration(sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.diemToan;
  document.getElementById("txtDiemLy").value = sv.diemLy;
  document.getElementById("txtDiemHoa").value = sv.diemHoa;
}
