var DSSV = "DSSV";

var dssv = [];

var dssvJson = localStorage.getItem(DSSV);
if (dssvJson != null) {
  var svArr = JSON.parse(dssvJson);
  dssv = svArr.map(function (item) {
    return new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.diemToan,
      item.diemLy,
      item.diemHoa
    );
  });
  renderDSSV(dssv);
}

function moreStudents() {
  var sv = informationFromSample();
  if (isVarLid) {
    dssv.push(sv);
    var dssvJson = JSON.stringify(dssv);
    localStorage.setItem(DSSV, dssvJson);
    renderDSSV(dssv);
  }
}

function deleteStudent(idSV) {
  var position = locationSearch(idSV, dssv);
  if (position != -1) {
    dssv.splice(position, 1);
    renderDSSV(dssv);
  }
}

function studentCorrection(idSv) {
  var position = locationSearch(idSv, dssv);
  if (position == -1) {
    return;
  }

  var sv = dssv[position];
  sampleInformationDemonstration(sv);
}

function studentUpdate() {
  var sv = informationFromSample();
  var position = locationSearch(sv.ma, dssv);
  if (position != -1) {
    dssv[position] = sv;
    renderDSSV(dssv);
  }
  moreStudents();
}
